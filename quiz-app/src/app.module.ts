import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; // Import du module HttpClient
import { QuizComponent } from './app/quiz/quiz.component';
import { AppRoutingModule } from './app/app.routes';
import { QuizService } from './app/quiz.service';
import { AppComponent } from './app/app.component';
import { QuestionComponent } from './app/question/question.component';


@NgModule({
  declarations: [
  AppComponent,
  QuizComponent,
  QuestionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule // Ajout de HttpClientModule dans les imports
    // Autres modules
  ],
  providers: [QuizService], // Déclaration du service QuizService dans les providers
  bootstrap: [AppComponent]
})
export class AppModule { }
