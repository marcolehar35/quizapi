import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { Quiz } from './model/quiz';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  private apiUrl = 'http://localhost:8081/quiz'; // Remplacez par l'URL de votre API

  constructor(private http: HttpClient) { }

  getQuiz(): Observable<any[]> {
    return this.http.get<any>(`${this.apiUrl}`).pipe(
      map(response => response._embedded.quiz)
    );
  }

  getQuestionsByQuizAstreeSoftware(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/1/questions`);
  }

  getQuestionsByQuiz4CADGROUP(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/2/questions`);
  }

  submitAnswers(answers: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/submit`, answers);
  }
}
