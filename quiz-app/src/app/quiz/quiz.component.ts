import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Quiz } from '../model/quiz';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrl: './quiz.component.css'
})
export class QuizComponent implements OnInit {
  quizs!: Quiz[];
  currentQuestionIndex: number = 0;
  score: number = 0;
  showResult: boolean = false;

  constructor(private quizService: QuizService) { }

  ngOnInit(): void {
    this.quizService.getQuiz().subscribe({
      next: data => {
        console.log(data)
        this.quizs = data;
      },
      error: error => {
        console.error('Erreur lors de la récupération des quiz:', error);
      },
      complete: () => {
        console.log('Récupération des quiz terminée');
      }
    });


  
  
}
}

