import { Component, Input, OnInit } from '@angular/core';
import { Quiz } from '../model/quiz';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrl: './question.component.css'
})
export class QuestionComponent   {

  @Input()
  data! : Quiz;
}
