export class Quiz {
    id: string;
    title: string;
    description: string;
    createdAt: string;

    constructor(title: string, description: string, createdAt : string, id : string) {
        this.title =title;
        this.description = description;
        this.id = id;
        this.createdAt = createdAt;
    }
}
