package org.company.QuizApi.repository;

import org.company.QuizApi.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "quiz", path ="quiz")
public interface QuizRepository extends JpaRepository<Quiz, Integer> {
}
