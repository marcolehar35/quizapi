package org.company.QuizApi.repository;

import org.company.QuizApi.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "answer", path ="answer")
public interface AnswerRepository extends JpaRepository<Answer, Integer> {


}
