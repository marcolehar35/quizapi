
-- Ajouter le thème du quiz Java
INSERT INTO devtools.quiz (title, description) VALUES ('Astrée Software', 'Une innovation est belle lorsqu''elle est simple');
INSERT INTO devtools.quiz (title, description) VALUES ('4CAD GROUP', 'Nous sommes les connexateurs');

    -- Ajouter les questions et les réponses
    INSERT INTO devtools.question (quiz_id, question_text) VALUES (1,'Qui est le nouveau Directeur Général d''Astrée Software ?');
    INSERT INTO devtools.answer ( question_id, answer_text, is_correct) VALUES (1,'Damien Lyant', TRUE);
    INSERT INTO devtools.answer ( question_id, answer_text, is_correct) VALUES (1,'Pierrick BESNET', FALSE);

     INSERT INTO devtools.question (quiz_id, question_text) VALUES (2, 'Que signifie "les Connexateurs" ?');
     INSERT INTO devtools.answer ( question_id, answer_text, is_correct) VALUES (1,'Contraction de connexion (numérique) et d''accélérateur, activateur, agitateur, créateur, acteur, accompagnateur, le Connexateur est au coeur de vos enjeux industriels pour connecter les systèmes numériques entre eux.', TRUE);
     INSERT INTO devtools.answer ( question_id, answer_text, is_correct) VALUES (1,'Logiciel de mise en connexion entre les machines de production industrielle', FALSE);